# LaTeX document styleguide

The one tenet of this guide is that **LaTeX document is a codebase like any other**, and therefore needs rules and care like any other well-maintained codebase.
This involves formatting, style, conventions, linting and testing.

This is a work-in-progress document, suggestions welcome.

## General provisions

This section is a top-level overview of guidelines.

## Errors

LaTeX document must not have errors.
`--interaction=nonstopmode` must never be used (despite it being default in Overleaf).
Document generation with errors is effectively undefined.

## Choice of IDE

This guide does not prescribe an IDE, but the author uses [VS Code](https://code.visualstudio.com/), and with its LaTeX extension it seems to do the best job on the market even compared to Overleaf and TexShop.
Syntax highlight, linting, compilation, snippets and even synctex are supported.
This guide does note that for a large LaTeX document, like a research paper, having a locally run IDE as opposed to Overleaf is beneficial.
The guide also recommends using a PDF viewer that has synctex support, such as [Skim app](https://skim-app.sourceforge.io/).

## Versioning

As any code deliverable, LaTeX document must be versioned.
De-facto standard of versioning is Git, and one should host the repository in a remote service, such as GitLab or Github.
Cool benefit: Overleaf internally stores project as git repositories, so one can (and should) easily mirror/sync Gitlab/Github repository with Overleaf.

### Tabs vs spaces

This choice, though made almost religious by many, often depends on language itself.
In this guide, the recommended indentation mechanism is **tabs**.

The arguments for tabs are often that tabs display width is configurable, and that tab "stops" at a predefined distance from line start instead of always printing a fixed number of spaces.
Both these properties are useful in a LateX document in particular (especially tabular environments).
The choice of tab width is less strict but must be set upfront.
This guide recommends a **tab width of 4**.

### Linting

Linting needs to be a routine and enforceable part of document development.
Classic LaTeX linter is [`chktex`](https://www.nongnu.org/chktex/).
IT can be run from CLI and is well-integrated into IDEs.
One can configure what warnings / errors to report and those can ignored on individual lines.
As preliminary measure, all `chktex` warnings should be treated as errors.

As a part of linting, we recommend running a spell checker (like [cSpell](https://cspell.org/)) over the code at every update.

### Testing

Surprisingly, LaTeX output, a.k.a. PDF, can be tested.
Suggested tests are

1. Validating that all fonts are embedded.
2. Validating that all rendered graphics are vectorized (if intended).
3. Validating PDF size and some metadata.
4. Extracting statistics such number of pages, grade level fo text, GPT score, etc.

## Project structure

Every LaTeX document must have a `main.tex` file.
However, it is almost never enough to have just this one file.
This guide suggests to have **the following structure**.

```text
├── Dockerfile (optional, can be used to construct container with PDF)
├── README.md (what is this document?)
├── build.sh (script to compile; usually more involved than one command)
├── document
│   ├── abstract.tex (optional)
│   ├── acknowledgements.tex (optional)
│   ├── bibfile.bib
│   ├── dist (the directory where PDF and aux files will be)
│   ├── figures (LaTeX files to be included that contain tables, figures, algorithms, etc)
│   │   ├── tbl-smth.tex
│   │   ├── alg-smth.tex
│   │   └── fig-smth.tex
│   ├── graphics (directory with graphics to embed)
│   │   └── picture.pdf
│   ├── main.tex (main file with \documentclass)
│   ├── meta.tex (optional; variable data, like author names, dates, IDs, numbers)
│   ├── preamble.tex (where you include packages and define macros)
│   └── sections
│       ├── section-1.tex
│       ├── ...
│       └── section-last.tex
└── lint.sh (run linters on code)
```

Here is how main file would look like:

```tex
% main.tex

\documentclass{article}

	\input{preamble}

	\begin{document}

		\input{meta}

		\maketitle

		\begin{abstract}
			\input{abstract}
		\end{abstract}

		\input{sections/section-1}
		\input{sections/...}
		\input{sections/section-last}

		\printbibliography%

	\end{document}
```

Note, **all names follow linux filename conventions**: lowercase and dashes (`lowercase-and-dashes.tex`).

## Indentation and spacing

We have noted above that the spacing of choice is **tabs of width 4**.
Now we turn to indentation in general.

First, **every logical block of code must be indented once**.
If the block has a closing element (such as `\end{...}` or `}`), that block needs to be de-indented to the level of opening block.
Some blocks may not have closing components, such as sections (`\section{...}`, etc).
For example:

```tex
\section{Name}

	Hello there!

	\begin{equation}
		a x^2 + b x + c = 0
	\end{equation}

	\myMacro{
		$a x^2 + b x + c = 0$
	}
```

Second, **every sentence has to start at new line**, no exceptions.

```tex
\section{Name}

	Here is the sentence.
	Another sentence even if it is super long.
	By the way, that is why good IDE with horizontal scroll is recommended.
	Also makes it easier to version sentences since git works line by line.
	Even\footnote{footnotes} are not exceptions.
	If footnote is\footnote{
		very long or constitutes a full sentence
	} here is how to do it.

```

Third, **spacing between paragraphs and blocks of code is one empty line**, not more.

Fourth, in general all tokens which can be separated by spacing should be separated by single space.
For example:

```tex
a x^2 + b x + c = 0 % good
ax^2 + bx + c = 0 % bad
a  x^2 +   b x +  c = 0 % bad
```

### Tabular

Tabular environments (defined as those with `&` separators) deserve a separate section.
This is where tabs come to rescue.
The rule is every `&` starts at the same tabular indentation.
Also, separator is followed by a single space for cell content.

```tex
\begin{center}
	\begin{tabular}{ c c c }
		cell1	& cell2	& cell3	\\
		cell4	& cell5	& cell6	\\
		cell7	& cell8	& cell9
	\end{tabular}
\end{center}
```

Same goes for tabular environments like `split`.

```tex
\begin{equation}
	\begin{split}
		A	& = \frac{\pi r^2}{2}	\\
 			& = \frac{1}{2} \pi r^2
	\end{split}
\end{equation}
```

Same goes to more complex tabular environments like algorithms and protocols.

## Using proper tools for certain tasks

In this section we cover common tasks document writer do, and suggestions on how to do them right in LaTeX.

### Highlighting

Proper highlighting is using `\emph` (NOT `\textit`) for pieces of text one want to emphasize.
Bold text (`\textbf`) should be rare an is more appropriate to highlight figures and numbers, like `Improvement of \textbf{30 times}.`.
Underline and color highlights should never be used.
Color text is appropriate in draft version to highlight collaborators' comments.

In general, function calls should be formatted with `\textsc{FuncCall}`.
Variable names are `\texttt{variable}` if the variable is more than one character, otherwise mathematical notation is allowed (`$x$`).
Variable names in subscripts and superscripts should use `\mathsf{variable}` or mathematical mode for single character.

```tex
\[
	\texttt{variable} \gets \mathsf{SquareRoot}(x + y_{\mathsf{variable}})
\]
```

### Units

In almost all cases where `siunitx` can be used, it should be used.
This includes plan numbers (especially if they are 4+ digits longs).
Here are common examples of siunitx uses.

1. SI measurements: kg, cm, m/s, degree celsius, **percentage**.
2. Data measurements: KiB, MB, TB/s.
3. Plain numbers: thousands separator after 3 digits.
4. Applying unit wrapper on all cells of column in a table.

Read [siunitx manual](https://mirrors.rit.edu/CTAN/macros/latex/contrib/siunitx/siunitx.pdf).

### Tables

[Book tabs](https://ctan.org/pkg/booktabs?lang=en) should be used instead of horizontal lines.
Vertical lines should never be used.
Where possible, one should try to apply formatting to whole column as opposed to cells individually.

```tex
\begin{tabular}{c >{\em}c}
	foo	& emp	\\
	foo	& emp
\end{tabular}
```

Where possible and makes sense, table should be scaled to occupy exactly the width of column or page.
However, table should never be scaled with `adjustbox` or similar that just scales the vectorized content.
Instead, only the width should stretch, like when one column is designated as floating-width.

```tex
\begin{longtable}{@{\extracolsep{\fill}} c c l c @{}}
...
\end{longtable}
```

### References

Everything that can be automated, should be automated.
This especially applies to referencing in LaTeX.

In general, there must be no single hard-coded references (section name, equation number, algorithm line, etc) in he document.
On top of obvious use of `\ref{labe}`, there is a [cleveref](https://texdoc.org/serve/cleveref/0) package that can let you reference objects by label alone.

```tex
\section{My section}\label{section:my-section}

	We see in \cref{section:my-section} that ...

\end{document}
```

On labels, all labels should be ina form of `label-type:label-name`.
The types are `section`, `figure`, `algorithm`, `table`, etc.
Where sections are nested, it is recommended to have nested labels: `section:section-name:subsection-name`.

Automation also extends to line numbers in algorithms, section names (as opposed to only numbers), and bibliographical data, such as author names.

### Itemization and enumeration

Wherever there is an enumeration in a sentence, inline enumeration should be used.

```tex
I want to eat
\begin{enumerate*}
	\item apples,
	\item bananas, and
	\item berries
\end{enumerate*}.
```

If a specific label type is desired, it should be specified as an option to the whole list.

### Macros

Macros are friends and should be useful everywhere where there is a repetition, big or small.
Macros should be defined in `preamble.tex` and should be well-documented, especially with respect to inputs.
Macros should be names in PascalCase.
Note, macros can easily be nested.

### Miscellaneous

There should be no commented-out code.
Comments are meant to explain the code, not to leave behind a draft.
If you want to leave the code but not include it in the document (temporary or depending on circumstance), use `\if` statements.

```tex
\ifshaphered
	The code.
\fi
```
